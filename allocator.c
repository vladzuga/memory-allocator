#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "arena.h"

int n;
unsigned char *v = NULL;

struct params {
	int current, next, current_size;
};

int getnr (char *s) {
	int i = strlen(s)-1, j, nr = 0;
	for (j = 0; j <= i; j++) {
		nr *= 10;
		nr += s[j] - '0';
	}
	return nr;
}

struct params search_free_block (int size) {//returnez un struct cu valorile current, current_size si next
	struct params a;
	a.current = 0;
	int found = 0;
	a.current_size = 0;
	
	a.next = *(int *)(v);
	
	//cazul 0 : arena e goala
	if ( a.next == 0 )
		return a;
	
	//cazul 1 : blocul se gaseste intre indexul de start si primul bloc alocat
	if ( a.next - sizeof(int) >= size + 3*sizeof(int) )
		return a;
	
	a.current = a.next;
	//cazul 2 : blocul se gaseste intre 2 blocuri consecutive
	a.next = *(int *)(v+a.current);
	a.current_size = a.current + (*(int *)(v+a.current+2*sizeof(int))); // practic, indexul ultimului octet alocat
	while ( a.next != 0 && !found ) {
		if ( a.next - a.current_size >= size + 3*sizeof(int) ) {
			found = 1;
			return a;
		}
		a.current = a.next;
		a.next = *(int *)(v+a.current);
		a.current_size = a.current + (*(int *)(v+a.current+2*sizeof(int)));
	}
	
	//cazul 3 : blocul se gaseste intre ultimul bloc si finalul arenei
	if ( n - a.current_size >= size + sizeof(int)*3 ) {
		a.next = -1;
		return a;
	}
	
	//nu am gasit loc
	a.current = -1;
	return a;
}

void allocator (int size) {
	//int next, current, current_size;
	int tmp;
	struct params a;
	a = search_free_block(size);

	if ( a.next == -1 ) { // blocul liber se gaseste la finalul arenei, cazul 3
		*(int *)(v+a.current_size) = 0;
		*(int *)(v+a.current_size+sizeof(int)) = a.current;
		*(int *)(v+a.current_size+2*sizeof(int)) = size + 3*sizeof(int);
		
		*(int *)(v+a.current) = a.current_size;
		tmp = a.current_size + 3*sizeof(int);
		printf("%d\n", tmp);
		return;
	}
	if ( a.next == 0 && a.current == 0 ) { // cazul 0
		*(int *)v = sizeof(int);
		*(int *)(v+sizeof(int)) = 0;
		*(int *)(v+2*sizeof(int)) = 0;
		*(int *)(v+3*sizeof(int)) = size + 3*sizeof(int);
		tmp = 4*sizeof(int);
		printf("%d\n", tmp);
		return;
	}
	if ( a.next != 0 && a.current == 0 ) { //blocul liber se afla intre index si singurul bloc din arena, cazul 1
		*(int *)v = sizeof(int);
		*(int *)(v+sizeof(int)) = a.next;
		*(int *)(v+2*sizeof(int)) = 0;
		*(int *)(v+3*sizeof(int)) = size + 3*sizeof(int);
		*(int *)(v+a.next+sizeof(int)) = sizeof(int);
		tmp = 4*sizeof(int);
		printf("%d\n", tmp);
		return;
	}
	if ( a.current == -1 ) {
		printf("0\n");
		return;
	}
	*(int *)(v+a.current_size) = a.next; //primii 4 octeti din sectiunea de gestiune sunt setati la blocul de la octetul next
	*(int *)(v+a.current_size+sizeof(int)) = a.current; //urmatorii 4 octeti sunt setati la blocul de la octetul current
	*(int *)(v+a.current_size+2*sizeof(int)) = size + 3*sizeof(int); //ultimii 4 octeti sunt setati la dimensiunea totala a blocului
	
	*(int *)(v+a.current) = a.current_size; //primii 4 octeti ai blocului precedent sunt setati la indexul blocului nou alocat
	*(int *)(v+a.next+sizeof(int)) = a.current_size; //octetii rezervati indicelui blocului precedent ai blocului ce succede blocul alocat sunt setati la indexul blocului nou alocat SCHIMBA COMENTARIUL - PREA NECLAR
	tmp = a.current_size+3*sizeof(int);
	printf("%d\n", tmp);
}

void fill (int index, int size, int value) {
	int i;
	for ( i = index; i <= index+size-1 && i < n; i ++ ) {
		*(unsigned char *)(v+i) = value;
	}
}

void dump() {
	int i, j;
	for ( i = 0; i < n; i +=16 ) {
		printf("%08X\t", i);
		for ( j = i; j < i+15 && j < n; j ++ ) { // nu afisez spatiu la sfarsitul liniei
			printf("%02X ", v[j]);
			if ( j == i+7 )
				printf(" ");
		}
		if ( j < n ) // daca nu am ajuns la ultimul bit afisez ultimul octet urmat de newline
			printf("%02X\n", v[j]);
	}
	printf("\n%08X\n", n);
}

void Free (int index) {
	int prev, next, current;
	current = index - 3*sizeof(int);
	next = *(int *)(v+current);
	prev = *(int *)(v+current+sizeof(int));
	
	if (prev)
		*(int *)(v+prev) = next;
	else
		*(int *)(v) = next;

	if (next)
		*(int *)(v+next+sizeof(int)) = prev;
}

/*
1 - FREE
2 - USAGE
3 - ALLOCATIONS
*/
void show (int param) {
	int current, next, tmp;
	current = *(int *)(v);
	next = *(int *)(v+current);
	int nblocks, nblocks_free, nbytes_occupied, nbytes_used, nbytes_alloc, res;
	float x;
	
	
	
	if ( param == 1 ) {
		nblocks_free = 0, nbytes_occupied = sizeof(int);
		
		if ( current == 0 ) { // niciun bloc alocat
			tmp =  n - sizeof(int);
			printf("1 blocks (%d bytes) free\n", tmp);
			return;
		}
			tmp = 0;
		if ( current - sizeof(int) >= 1 )
			nblocks_free += 1, tmp += current - sizeof(int);
		if ( next == 0 ) { // un singur bloc alocat
			if ( n - current - (*(int *)(v+current+2*sizeof(int))) >= 1 )
				nblocks_free += 1, tmp += n - current - (*(int *)(v+current+2*sizeof(int)));
			printf("%d blocks (%d bytes) free\n", nblocks_free,  tmp);
			return;
		}
		
		while ( next != 0 ) { // mai multe blocuri alocate
			if ( next - current - (*(int *)(v+current+2*sizeof(int))) >= 1 )
				nblocks_free ++;
			nbytes_occupied += *(int *)(v+current+2*sizeof(int));
			
			current = next;
			next = *(int *)(v+current);
		}
		nbytes_occupied += *(int *)(v+current+2*sizeof(int));
		if ( n - *(int *)(v+current+2*sizeof(int)) - current >= 1 ) // daca mai am octeti liberi dupa ultimul bloc alocat
			nblocks_free ++;
		printf("%d blocks (%d bytes) free\n", nblocks_free, n - nbytes_occupied);
		return;
	}
	
	/////////////////////////////////////
	
	if ( param == 2 ) {
		if ( current == 0 ) { // niciun bloc alocat
			printf("0 blocks (0 bytes) used\n");
			printf("0%% efficiency\n");
			printf("0%% fragmentation\n");
			return;
		}
		if ( next == 0 ) { // un singur bloc alocat
			tmp = ( *(int *)( v+current+2*sizeof(int) ) ) - 3*sizeof(int);
			printf("1 blocks (%d bytes) used\n", tmp);
			
			nbytes_used = *(int *)(v+current+2*sizeof(int)) - 3*sizeof(int);//octeti folositi
			nbytes_alloc = nbytes_used + 4*sizeof(int);//octeti folositi + sectiunea de gestiune + indexul = octeti care nu sunt liberi
			x = ( nbytes_used*1.0 )/( nbytes_alloc ) * 100;
			res = (int)x;
			printf("%d%% efficiency\n", res);
			
			nblocks_free = 1;
			if ( current - sizeof(int) >= 1 )
				nblocks_free += 1;
			tmp = ( nblocks_free - 1 ) * 100;
			printf("%d%% fragmentation\n", tmp);
			return;
		}
		
		nblocks_free = nbytes_occupied = nblocks = 0;
		if ( current - sizeof(int) >= 1 )
				nblocks_free += 1;
		while ( next != 0 ) { // mai multe blocuri alocate
			nblocks ++;
			if ( next - current - (*(int *)(v+current+2*sizeof(int))) >= 1 )
				nblocks_free ++;
			nbytes_occupied += *(int *)(v+current+2*sizeof(int)) - 3*sizeof(int);
			
			current = next;
			next = *(int *)(v+current);
		}
		nbytes_occupied += *(int *)(v+current+2*sizeof(int)) - 3*sizeof(int);
		nblocks ++;
		if ( n - current - (*(int *)(v+current+2*sizeof(int))) >= 1 )
				nblocks_free ++;
		printf("%d blocks (%d bytes) used\n", nblocks, nbytes_occupied);
		
		x = ( nbytes_occupied*1.0 )/( nbytes_occupied + nblocks*3*sizeof(int) + sizeof(int) )*100;
		res = (int)x;
		printf("%d%% efficiency\n", res);
		
		x = ( (nblocks_free - 1)*1.0 )/( nblocks )*100;
		res = (int)x;
		printf("%d%% fragmentation\n", res);
		return;
	}
	
	/////////////////param == 3
	
	printf("OCCUPIED 4 bytes\n");
	if ( current == 0 ) { // niciun bloc alocat in arena
		tmp = n - sizeof(int);
		printf("FREE %d bytes\n", tmp);
		return;
	}
	if ( next == 0 ) { // un singur bloc alocat in arena
		if ( current - sizeof(int) >= 1 )
			tmp = current - sizeof(int), printf("FREE %d bytes\n", tmp);
		tmp = *(int *)(v+current+2*sizeof(int)), printf("OCCUPIED %d bytes\n", tmp);
		if ( ( tmp = n - current - (*(int *)(v+current+2*sizeof(int))) ) >= 1 )
			printf("FREE %d bytes\n", tmp);
		return;
	}
	
	//mai multe blocuri alocate
	
	if ( current - sizeof(int) >= 1 )
			tmp = current - sizeof(int), printf("FREE %d bytes\n", tmp);
	while ( next != 0 ) {
		tmp = *(int *)(v+current+2*sizeof(int)), printf("OCCUPIED %d bytes\n", tmp);
		if ( ( tmp = next - current - (*(int *)(v+current+2*sizeof(int))) ) >= 1 )
			printf("FREE %d bytes\n", tmp);
		
		current = next;
		next = *(int *)(v+current);
	}
	//afiseaza si ultimul bloc
	tmp = *(int *)(v+current+2*sizeof(int));
	printf("OCCUPIED %d bytes\n", tmp);
	if ( ( tmp = n - current - (*(int *)(v+current+2*sizeof(int))) ) >= 1 ) // daca a mai ramas loc liber in arena dupa ultimul bloc alocat
		printf("FREE %d bytes\n", tmp);
}

int get_command_show (char *s) {
	if ( strcmp(s, "FREE") == 0 )
		return 1;
	if ( strcmp(s, "USAGE") == 0 )
		return 2;
	return 3;
}

/*
a = 
1 - initialize
2 - finalize
3 - dump
4 - alloc
5 - Free
6 - fill
7 - show
s = stringul ce urmeaza dupa comanda (parametri) (fara " ")
*/
void command(int a, char *s) {
	if ( a == 1 ) {
		initialize(n = getnr(s));
		return;
	}
	if ( a == 2 ) {
		finalize();
		return;
	}
	if ( a == 3 ) {
		dump();
		return;
	}
	if ( a == 4 ) {
		allocator(getnr(s));
		return;
	}
	if ( a == 5 ) {
		Free(getnr(s));
		return;
	}
	if ( a == 6 ) {
		char *s2;
		int nr;
		
		s2 = strtok(s, " ");
		s2 = strtok(NULL, " \n");
		nr = getnr(s2);
		s2 = strtok(NULL, " \n");
		
		fill(getnr(s), nr, getnr(s2));
		return;
	}
	
	//case 7-show
	char *p;
	p = strtok(s, " \n");
	show( get_command_show(p) );
}

int main () {
	char s[40], *p, *tmp;
	
	fgets(s, 40, stdin);
	p = strtok(s, " \n");
	while ( strcmp(p, "FINALIZE") != 0 ) {
		if ( strcmp(p, "DUMP") == 0 )
			command(3, NULL);
		else {
			tmp = strtok(NULL, "\n");
			if ( strcmp(p, "INITIALIZE") == 0 )
				command(1, tmp);
			if ( strcmp(p, "ALLOC") == 0 )
				command(4, tmp);
			if ( strcmp(p, "FREE") == 0 )
				command(5, tmp);
			if ( strcmp(p, "FILL") == 0 )
				command(6, tmp);
			if ( strcmp(p, "SHOW") == 0 )
				command(7, tmp);			
		}
		fgets(s, 40, stdin);
		p = strtok(s, " \n");
	}
	finalize(v);

	return 0;
}
