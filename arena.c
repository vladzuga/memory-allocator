#include <stdlib.h>
#include <string.h>
#include "arena.h"

extern unsigned char *v;

void initialize (int size) {
	v = malloc(size);
	memset(v, 0, size);
}

void finalize () {
	free(v);
}
